#!/bin/sh

set -ex

export IGT_FORCE_DRIVER=${DRIVER_NAME}
export PATH=$PATH:/igt/bin/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/igt/lib/aarch64-linux-gnu/:/igt/lib/x86_64-linux-gnu:/igt/lib

# Uncomment the below to debug problems with driver probing
: '
ls -l /dev/dri/
cat /sys/kernel/debug/devices_deferred
cat /sys/kernel/debug/device_component/*
'

# Cannot use HWCI_KERNEL_MODULES as at that point we don't have the module in /lib
if [ "$IGT_FORCE_DRIVER" = "amdgpu" ]; then
    mv /install/modules/lib/modules/* /lib/modules/.
    modprobe amdgpu
fi

igt_runner --version || true
igt_runner --test-list /install/local/${DRIVER_NAME}.testlist /igt/libexec/igt-gpu-tools/ -o /results
jq -r '.tests | to_entries | .[] | [.key, .value.result] | @csv' < /results/results.json | tr -d '"' > /results/results.txt

sed -r 's/(dmesg-warn|pass)/success/g' /results/results.txt > /results/results_simple.txt

if [ "${GPU_VERSION}" ]; then
    RESULTS_FILE=${DRIVER_NAME}_${GPU_VERSION}_results.txt
else
    RESULTS_FILE=${DRIVER_NAME}_results.txt
fi

if diff -q /install/local/${RESULTS_FILE} /results/results.txt; then
    cd $oldpath
    exit 0
fi

echo Unexpected change in results, resync baseline with patch -p0:
diff --label /results/results.txt --label .gitlab-ci/local/${RESULTS_FILE} -u /install/local/${RESULTS_FILE} /results/results.txt

cd $oldpath
exit 1
